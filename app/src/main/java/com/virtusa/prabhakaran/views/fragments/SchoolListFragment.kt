package com.virtusa.prabhakaran.views.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.virtusa.R
import com.android.virtusa.databinding.FragmentSchoolListBinding
import com.google.gson.Gson
import com.virtusa.prabhakaran.db.SchoolDatabase
import com.virtusa.prabhakaran.listener.AdapterClickListener
import com.virtusa.prabhakaran.util.Logger
import com.virtusa.prabhakaran.viewmodel.DataViewModel
import com.virtusa.prabhakaran.views.adapter.SchoolListAdapter
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


/**
 * Created by prabhakaranpanjalingam on 16,Mar,2023
 */

class SchoolListFragment : Fragment() ,AdapterClickListener{
    private  val TAG = "SchoolListFragment"
    private val dataViewModel by viewModel<DataViewModel>()
    private lateinit var mViewDataBinding: FragmentSchoolListBinding
    private val schoolListAdapter: SchoolListAdapter by inject()
    private val schoolDatabase:SchoolDatabase by inject()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding  = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_school_list, container, false
        )
        val mRootView = mViewDataBinding.root
        mViewDataBinding.lifecycleOwner = this
        return mRootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewDataBinding.viewModel = dataViewModel
        dataViewModel.getAllSchoolsList()
        mViewDataBinding.rvSchools.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        mViewDataBinding.rvSchools.adapter = schoolListAdapter

        schoolListAdapter.setAdapterClickListener(this)

        dataViewModel.schoolList.observe(viewLifecycleOwner, Observer {
            Logger.d(TAG,Gson().toJson(it))
              schoolListAdapter.submitList(it)

              dataViewModel.getSchoolMarksDetails()


        })
        dataViewModel.schoolMarks.observe(viewLifecycleOwner, Observer {
            Logger.d(TAG,Gson().toJson(it))


        })

    }

    override fun setMarks(ibn: String) {
         val bundle = Bundle()
        val schoolMarks = schoolDatabase.schoolDataDao.getSchoolMarkDetails(ibn)
        bundle.putParcelable("Marks",schoolMarks)
        Navigation.findNavController(mViewDataBinding.root).navigate(R.id.action_schoolListFragment_to_schoolMarksFragment,bundle)
    }


}

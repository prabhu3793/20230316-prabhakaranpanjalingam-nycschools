package com.virtusa.prabhakaran.views.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.virtusa.databinding.SchoolsItemBinding
import com.virtusa.prabhakaran.listener.AdapterClickListener
import com.virtusa.prabhakaran.model.SchoolListResponseItem

/**
 * Created by prabhakaranpanjalingam on 18,Mar,2023
 */


class SchoolListAdapter() : RecyclerView.Adapter<SchoolListAdapter.MediaViewHolder>() {
     private lateinit var  adapterClickListener:AdapterClickListener

    private val diffCallback: DiffUtil.ItemCallback<SchoolListResponseItem> =
        object : DiffUtil.ItemCallback<SchoolListResponseItem>() {
            override fun areItemsTheSame(
                oldItem: SchoolListResponseItem,
                newItem: SchoolListResponseItem
            ): Boolean {
                return oldItem.dbn.equals(newItem.dbn)
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(
                oldItem: SchoolListResponseItem,
                newItem: SchoolListResponseItem
            ): Boolean {
                return oldItem.dbn === newItem.dbn
            }
        }
    var schoolsList: AsyncListDiffer<SchoolListResponseItem> = AsyncListDiffer(this, diffCallback)
    fun submitList(messageList: List<SchoolListResponseItem>) {
        schoolsList.submitList(messageList)
    }

    fun setAdapterClickListener(adapterClickListener: AdapterClickListener){
        this.adapterClickListener = adapterClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        val adapterMediaBinding = SchoolsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MediaViewHolder(adapterMediaBinding)
    }

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        val mediaDetails: SchoolListResponseItem = schoolsList.currentList[position]
        holder.bind(mediaDetails)
    }

    override fun getItemCount(): Int {
        return schoolsList.currentList.size
    }

    inner class MediaViewHolder(val binding: SchoolsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(schoolListResponseItem: SchoolListResponseItem) {
            binding.schoolListData = schoolListResponseItem
            binding.executePendingBindings()
            binding.root.setOnClickListener(View.OnClickListener {
              adapterClickListener.setMarks(schoolListResponseItem.dbn)
            })

        }

    }
}
package com.virtusa.prabhakaran.views.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.virtusa.R
import com.android.virtusa.databinding.FragmentSchoolListBinding
import com.android.virtusa.databinding.FragmentSchoolMarkBinding
import com.google.gson.Gson
import com.virtusa.prabhakaran.model.SchoolMarksListItem
import com.virtusa.prabhakaran.util.Logger
import com.virtusa.prabhakaran.viewmodel.DataViewModel
import com.virtusa.prabhakaran.views.adapter.SchoolListAdapter
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


/**
 * Created by prabhakaranpanjalingam on 18,Mar,2023
 */

class SchoolMarksFragment : Fragment(){
    private  val TAG = "SchoolMarksFragment"
    private lateinit var mViewDataBinding: FragmentSchoolMarkBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding  = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_school_mark, container, false
        )
        val mRootView = mViewDataBinding.root
        mViewDataBinding.lifecycleOwner = this
        return mRootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewDataBinding.schoolMarksData = requireArguments().getParcelable("Marks")


    }







}

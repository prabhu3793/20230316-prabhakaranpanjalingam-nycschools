package com.virtusa.prabhakaran.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem
import com.virtusa.prabhakaran.repository.DataRepository
import com.virtusa.prabhakaran.util.AppResult
import com.virtusa.prabhakaran.util.SingleLiveEvent
import kotlinx.coroutines.launch


/**
 * Created by prabhakaranpanjalingam on 16,Mar,2023
 */

class DataViewModel(private val repository: DataRepository) : ViewModel() {

    val showLoading = ObservableBoolean()
    val schoolList = MutableLiveData<List<SchoolListResponseItem>>()
    val schoolMarks = MutableLiveData<List<SchoolMarksListItem>>()
    val showError = SingleLiveEvent<String>()


    fun getAllSchoolsList() {

        showLoading.set(true)
        viewModelScope.launch {
            val result = repository.getSchoolListDetails()

            showLoading.set(false)
            when (result) {
                is AppResult.Success -> {
                    schoolList.value = result.successData
                    showError.value = null
                }
                is AppResult.Error -> showError.value = result.exception.message
            }
        }
    }

    fun getSchoolMarksDetails(){
        showLoading.set(true)
        viewModelScope.launch {
            val result = repository.getSchoolsMarks()
            showLoading.set(false)
            when (result) {
                is AppResult.Success -> {
                    val data = result.successData
                    schoolMarks.value = data
                    showError.value = null
                }
                is AppResult.Error -> showError.value = result.exception.message
            }
        }
    }


}
package com.virtusa.prabhakaran

import androidx.multidex.MultiDexApplication
import com.virtusa.prabhakaran.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */


open class SchoolApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initiateKoin()
    }
    private fun initiateKoin() {
        startKoin{
            androidContext(this@SchoolApplication)
            modules(provideDependency())
        }
    }

    open fun provideDependency() = appComponent

}
package com.virtusa.prabhakaran.di

import com.virtusa.prabhakaran.viewmodel.DataViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

val viewModelModule = module {

    viewModel {
        DataViewModel(repository = get())
    }

}
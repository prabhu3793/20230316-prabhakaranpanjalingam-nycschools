package com.virtusa.prabhakaran.di

import com.virtusa.prabhakaran.db.SharedPref
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

val sharedPrefModule = module {

    single { SharedPref(androidContext()) }


}
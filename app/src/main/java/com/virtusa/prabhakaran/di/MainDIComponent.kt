package com.virtusa.prabhakaran.di


/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

/**
 * Main dependency component.
 * This will create and provide required dependencies with sub dependencies.
 */
val appComponent = listOf( apiModule,
    viewModelModule,
    repositoryModule,
    networkModule,
    databaseModule,
    sharedPrefModule,
   adapterModule)
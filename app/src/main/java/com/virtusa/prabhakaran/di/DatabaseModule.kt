package com.virtusa.prabhakaran.di

import android.app.Application
import androidx.room.Room
import com.paypay.prabhakaran.db.SchoolDataDao
import com.virtusa.prabhakaran.db.SchoolDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

val databaseModule = module {

    fun provideDatabase(application: Application): SchoolDatabase {
       return Room.databaseBuilder(application, SchoolDatabase::class.java, "schoolsData")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }
    fun provideCurrencyRateDao(database: SchoolDatabase): SchoolDataDao {
        return  database.schoolDataDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideCurrencyRateDao(get()) }


}

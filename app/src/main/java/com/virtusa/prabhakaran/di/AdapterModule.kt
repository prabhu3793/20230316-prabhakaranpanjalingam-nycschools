package com.virtusa.prabhakaran.di

import com.virtusa.prabhakaran.views.adapter.SchoolListAdapter
import org.koin.dsl.module

/**
 * Created by prabhakaranpanjalingam on 18,Mar,2023
 */

val adapterModule = module {

    factory { SchoolListAdapter() }


}
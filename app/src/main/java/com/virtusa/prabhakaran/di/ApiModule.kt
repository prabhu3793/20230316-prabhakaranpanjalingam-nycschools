package com.virtusa.prabhakaran.di

import com.virtusa.prabhakaran.repository.SchoolDetailsApi
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

val apiModule = module {

    fun provideCountriesApi(retrofit: Retrofit): SchoolDetailsApi {
        return retrofit.create(SchoolDetailsApi::class.java)
    }
    single{ provideCountriesApi(get()) }


}
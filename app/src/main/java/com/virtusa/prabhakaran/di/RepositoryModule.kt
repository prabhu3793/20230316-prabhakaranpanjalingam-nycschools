package com.virtusa.prabhakaran.di

import com.virtusa.prabhakaran.repository.DataRepository
import com.virtusa.prabhakaran.repository.DataRepositoryImpl
import org.koin.dsl.module

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

val repositoryModule = module {

    fun provideDataRepository(): DataRepository {
        return DataRepositoryImpl()
    }
    factory { provideDataRepository() }

}
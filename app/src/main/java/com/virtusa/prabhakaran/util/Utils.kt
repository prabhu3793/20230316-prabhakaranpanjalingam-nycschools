package com.virtusa.prabhakaran.util

import retrofit2.Response

object Utils {


    fun <T : Any> handleApiError(resp: Response<T>): AppResult.Error {
        val error = ApiErrorUtils.parseError(resp)
        return AppResult.Error(Exception(error.message))
    }

    fun <T : Any> handleSuccess(response: Response<T>): AppResult<T> {
        response.body()?.let {
                return if (response.isSuccessful){
                    AppResult.Success(it)
                }else if(!response.isSuccessful){

                        returnError(response.message())
                    }else{
                        returnError()
                    }
        } ?: return handleApiError(response)
    }


    private fun returnError(error:String?="Something went wrong"): AppResult.Error {
        return AppResult.Error(Exception(error))
    }
}
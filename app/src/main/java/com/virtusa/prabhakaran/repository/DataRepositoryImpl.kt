package com.virtusa.prabhakaran.repository

import android.content.Context
import com.paypay.prabhakaran.db.SchoolDataDao
import com.virtusa.prabhakaran.util.Logger
import com.virtusa.prabhakaran.db.SharedPref
import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem
import com.virtusa.prabhakaran.util.AppResult
import com.virtusa.prabhakaran.util.NetworkManager.isOnline
import com.virtusa.prabhakaran.util.TAG
import com.virtusa.prabhakaran.util.Utils.handleApiError
import com.virtusa.prabhakaran.util.Utils.handleSuccess
import com.virtusa.prabhakaran.util.noNetworkConnectivityError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.TimeUnit
/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

class DataRepositoryImpl(

) :
    DataRepository, KoinComponent {
    val api: SchoolDetailsApi by inject()
    val prefUtility: SharedPref by inject()
    val context: Context by inject()
    val schoolDao: SchoolDataDao by inject()

    override suspend fun getSchoolListDetails(): AppResult<List<SchoolListResponseItem>> {
       val localData = getSchoolListFromCache()
        val isSyncRequired = isSyncRequired()
        if (!isSyncRequired && !localData.isNullOrEmpty()) {
            return AppResult.Success(localData)
        }
        return if (isOnline(context)) {
            val data = getSchoolListDetailsFromApi()
            when (data) {
                is AppResult.Success -> {
                    withContext(Dispatchers.IO) {
                        schoolDao.insertSchools(data.successData)
                        prefUtility.saveLastSyncedTime(System.currentTimeMillis())
                    }
                }

            }
            return data


        } else {
            context.noNetworkConnectivityError()
        }
    }


    override suspend fun getSchoolListDetailsFromApi(): AppResult<List<SchoolListResponseItem>> {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.getSchoolList()
                if (response.isSuccessful) {

                     when (val result = handleSuccess(response)) {
                        is AppResult.Success -> {
                            AppResult.Success(result.successData)
                        }
                        is AppResult.Error -> {
                            AppResult.Error(Exception(result.message))
                        }
                    }

                } else {
                    handleApiError(response)
                }
            }
        } catch (e: Exception) {
            AppResult.Error(e)
        }
    }

    override suspend fun getSchoolsMarks(): AppResult<List<SchoolMarksListItem>> {
        val localData = getSchoolMarksFromCache()

       if(!localData.isNullOrEmpty()) {
            return AppResult.Success(localData)
        }
        return if (isOnline(context)) {
            val data = getSchoolsMarksFromApi()
            when (data) {
                is AppResult.Success -> {
                    withContext(Dispatchers.IO) {
                       schoolDao.insertSchoolMarks(data.successData)

                    }
                }

            }
            return data


        } else {
            return context.noNetworkConnectivityError()
        }

    }

    override suspend fun getSchoolsMarksFromApi(): AppResult<List<SchoolMarksListItem>> {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.getSchoolMarks()

                if (response.isSuccessful) {
                    when (val result = handleSuccess(response)) {
                        is AppResult.Success -> {
                            AppResult.Success(result.successData)
                        }
                        is AppResult.Error -> {
                            AppResult.Error(Exception(result.message))
                        }
                    }

                } else {
                    handleApiError(response)
                }
            }
        } catch (e: Exception) {
            AppResult.Error(e)
        }
    }


    private suspend fun getSchoolListFromCache():List<SchoolListResponseItem> {
        return withContext(Dispatchers.IO) {
            schoolDao.findSchoolListAll()
        }
    }

    private suspend fun getSchoolMarksFromCache():List<SchoolMarksListItem> {
        return withContext(Dispatchers.IO) {
            schoolDao.findSchoolMarksAll()
        }
    }

    /**
     * Returns true if sync required, otherwise will return false
     */
    private fun isSyncRequired(): Boolean {
        val currentTimestamp = System.currentTimeMillis()
        val savedTimestamp = getLastSyncedTime()
        val diff = TimeUnit.MINUTES.toMillis(60)
        val out = currentTimestamp - savedTimestamp > diff
        Logger.d(
            TAG,
            "isSyncRequired() : out=$out ct=$currentTimestamp st=$savedTimestamp diff=$diff"
        )
        return out
    }

    /**
     * Return last synced time
     */
    private fun getLastSyncedTime(): Long {
        return prefUtility.getLastSyncedTime()
    }
}
package com.virtusa.prabhakaran.repository

import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem
import retrofit2.Response
import retrofit2.http.GET

interface SchoolDetailsApi {


    @GET("/resource/s3k6-pzi2.json?")
    suspend fun getSchoolList(): Response<List<SchoolListResponseItem>>


    @GET("/resource/f9bf-2cp4.json?")
    suspend fun getSchoolMarks(): Response<List<SchoolMarksListItem>>


}
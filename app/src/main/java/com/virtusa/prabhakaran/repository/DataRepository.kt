package com.virtusa.prabhakaran.repository

import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem
import com.virtusa.prabhakaran.util.AppResult

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

interface DataRepository {
    suspend fun getSchoolListDetails() : AppResult<List<SchoolListResponseItem>>
    suspend fun getSchoolListDetailsFromApi() : AppResult<List<SchoolListResponseItem>>
    suspend fun getSchoolsMarks() : AppResult<List<SchoolMarksListItem>>
    suspend fun getSchoolsMarksFromApi() : AppResult<List<SchoolMarksListItem>>
}

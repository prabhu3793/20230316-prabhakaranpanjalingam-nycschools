package com.virtusa.prabhakaran.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "school_marks")
class SchoolMarksListItem : Parcelable {
    @PrimaryKey
    var dbn: String = ""
    var num_of_sat_test_takers: String = ""
    var sat_critical_reading_avg_score: String = ""
    var sat_math_avg_score: String = ""
    var sat_writing_avg_score: String = ""
    var school_name: String = ""
}
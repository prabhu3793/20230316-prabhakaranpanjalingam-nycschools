package com.paypay.prabhakaran.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */


@Dao
interface SchoolDataDao {

   @Query("SELECT * FROM school_list")
   fun findSchoolListAll(): List<SchoolListResponseItem>

    @Query("SELECT * FROM school_marks")
    fun findSchoolMarksAll(): List<SchoolMarksListItem>

    @Query("SELECT * FROM school_marks WHERE dbn=:dbn")
    fun getSchoolMarkDetails( dbn:String): SchoolMarksListItem

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchools(schoolList : List<SchoolListResponseItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchoolMarks(schoolsMarks: List<SchoolMarksListItem>)

}
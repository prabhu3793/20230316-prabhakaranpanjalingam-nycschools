package com.virtusa.prabhakaran.db

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences


/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

class SharedPref(context: Context) {
    private val applicationContext = context.applicationContext
    private val editor: SharedPreferences.Editor
    private val sharedPreferences: SharedPreferences
    init {

         sharedPreferences = applicationContext.getSharedPreferences(
            "MySharedPref",
            MODE_PRIVATE
        )
        editor = sharedPreferences.edit()

    }



     fun saveLastSyncedTime(lastSyncedTimeStamp: Long) {
        editor.putLong(LAST_SYNCED_TIME,lastSyncedTimeStamp)
         editor.commit()
    }

    fun getLastSyncedTime() :Long{

       return sharedPreferences.getLong(LAST_SYNCED_TIME,-1)
    }


    companion object {
        val LAST_SYNCED_TIME =  "last_synced"
    }
}
package com.virtusa.prabhakaran.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.paypay.prabhakaran.db.SchoolDataDao
import com.virtusa.prabhakaran.model.SchoolListResponseItem
import com.virtusa.prabhakaran.model.SchoolMarksListItem

/**
 * Created by prabhakaranpanjalingam on 17,Mar,2023
 */

@Database(
    entities = [SchoolListResponseItem::class, SchoolMarksListItem::class],
    version = 1, exportSchema = false
)

abstract class SchoolDatabase : RoomDatabase() {
    abstract val schoolDataDao: SchoolDataDao
}